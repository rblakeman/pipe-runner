# Pipe Runner

Fun test to see how fast it is to prototype a Flappy Bird clone.

All assets made by me. No copyright infringement intended.

> Unity Version 2019.2.11f1

Ryan Blakeman ©2020

rblakeman31@gmail.com

https://ryanblakeman.com

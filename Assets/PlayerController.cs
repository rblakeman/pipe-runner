﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
  public GameObject GameManager;
  public GameObject Camera;
  private Rigidbody2D rb2d;

  // Start is called before the first frame update
  void Start() {
    rb2d = GetComponent<Rigidbody2D>();
    rb2d.velocity = new Vector2(0.5f, 0f);
    Camera.GetComponent<Transform>().position = new Vector3(0f,0f,-10f);
  }

  // Update is called once per frame
  void Update() {
    Camera.GetComponent<Transform>().position = new Vector3(GetComponent<Transform>().position.x,0f,-10f);
    if (!getGameManagerPaused()) {
      if (!rb2d.simulated) rb2d.simulated = true;
      Vector2 newForce = new Vector2(0.5f, 0f);
      if (Input.GetKeyDown(KeyCode.Space)) {
        newForce.y = 300f;
      }
      rb2d.AddForce(newForce);
    } else {
      rb2d.simulated = false;
    }
  }

  bool getGameManagerPaused() {
    return GameManager.GetComponent<GameManager>().paused;
  }

  void setGameManagerPaused(bool newValue) {
    GameManager.GetComponent<GameManager>().paused = newValue;
  }

  void setGameManagerDead(bool newValue) {
    GameManager.GetComponent<GameManager>().dead = newValue;
  }

  int getGameManagerScore() {
    return GameManager.GetComponent<GameManager>().score;
  }

  void setGameManagerScore() {
    GameManager.GetComponent<GameManager>().score = getGameManagerScore() + 1;
  }

  void OnTriggerEnter2D(Collider2D other) {
    if (other.gameObject.tag == "WallDead") {
      setGameManagerDead(true);
      setGameManagerPaused(true);
    }
  }

  void OnTriggerExit2D(Collider2D other) {
    if (other.gameObject.tag == "WallScored") {
      setGameManagerScore();
    }
  }
}

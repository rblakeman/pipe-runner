﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
  public float startingWallPositionX = 5f;
  public float wallDistance = 2f;
  public GameObject map;
  public GameObject wallPairPrefab;
  public GameObject wallPrefab;

  public GameObject player;
  public Vector3 playerPosLastSpawn;
  public bool paused;
  public int score;
  public Text scoreText;
  public string HIGH_SCORE_TEXT = "highscore";
  public Text highScoreText;
  public bool dead;
  public Image gameOverUI;

  // Awake is called before Start
  void Awake() {
    if (!PlayerPrefs.HasKey(HIGH_SCORE_TEXT)) {
      PlayerPrefs.SetInt(HIGH_SCORE_TEXT, 0);
      Debug.Log("none resetting");
    }
    score = 0;
    paused = dead = false;
    playerPosLastSpawn = new Vector3(0f,0f,0f);
  }

  // Start is called before the first frame update
  void Start() {
    spawnWall();
    highScoreText.GetComponent<Text>().text = "High Score: " + PlayerPrefs.GetInt(HIGH_SCORE_TEXT);
  }

  // Update is called once per frame
  void Update() {
    if (dead) {
      gameOverUI.gameObject.SetActive(true);

      if (Input.GetKeyDown(KeyCode.R)) {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
      }
    } else {
      if (Mathf.Abs(getPlayerPos().x - playerPosLastSpawn.x) >= 5f) {
        spawnWall();
      }

      if (Input.GetKeyDown(KeyCode.Escape)) {
        paused = !paused;
      }

      scoreText.GetComponent<Text>().text = "Score: " + score;
      if (PlayerPrefs.GetInt(HIGH_SCORE_TEXT) < score) {
        PlayerPrefs.SetInt(HIGH_SCORE_TEXT, score);
        highScoreText.GetComponent<Text>().text = "High Score: " + PlayerPrefs.GetInt(HIGH_SCORE_TEXT);
      }
    }
  }

  Vector3 getPlayerPos() {
    return player.GetComponent<Transform>().position;
  }

  void spawnWall() {
    // TopPipe = 9 -> 3, BottomPipe = -3 -> -9
    int randomY = (int)(Random.value*7);
    Vector3 currentPlayerPos = getPlayerPos();
    GameObject wallPair = Instantiate(wallPairPrefab, new Vector3(currentPlayerPos.x + 10f,0f,0f), Quaternion.identity);
    GameObject newTopWall = Instantiate(wallPrefab, new Vector3(currentPlayerPos.x + 10f, 3f+randomY, 0f), Quaternion.identity);
    GameObject newBottomWall = Instantiate(newTopWall, new Vector3(currentPlayerPos.x + 10f, -9f+randomY, 0f), Quaternion.identity);
    newBottomWall.GetComponentInChildren<SpriteRenderer>().flipY = true;
    newTopWall.transform.parent = wallPair.transform;
    newBottomWall.transform.parent = wallPair.transform;
    wallPair.transform.parent = map.transform;
    wallPair.GetComponent<BoxCollider2D>().offset = new Vector2(0f, Mathf.Floor((newTopWall.GetComponent<Transform>().position.y + newBottomWall.GetComponent<Transform>().position.y) / 2f));
    playerPosLastSpawn = currentPlayerPos;
  }
}

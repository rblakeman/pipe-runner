﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteSelf : MonoBehaviour
{
  public GameObject player;

  // Start is called before the first frame update
  void Start()
  {
    player = GameObject.Find("Player");
  }

  // Update is called once per frame
  void Update()
  {
    if (getPlayerPos().x - GetComponent<Transform>().position.x > 10f) {
      Destroy(this.gameObject);
    }
  }

  Vector3 getPlayerPos() {
    return player.GetComponent<Transform>().position;
  }
}
